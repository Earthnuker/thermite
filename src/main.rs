#![feature(euclidean_division)]
extern crate image;
extern crate minifb;
extern crate num_cpus;
extern crate rand;
extern crate thermite;
use minifb::Key;
use thermite::Buffer;
use thermite::Flame;
use thermite::Point;
fn set_title(b: &mut Buffer, n: usize, spp: f64, v: (usize, usize), anim: bool) {
    let title = format!(
        "[{}] Gamma: {}, Exposure: {}, SPP: {}, Elements: {}, Element size: {}, Anim: {}",
        n, b.gamma, b.exposure, spp, v.0, v.1, anim
    );
    b.title(&title);
}

fn compute_point(f: &Flame, n: usize) -> Vec<Point> {
    let mut p = Point::rand();
    let mut ret = Vec::new();
    for _ in 0..20 {
        f.step(&mut p);
    }
    for _ in 0..n {
        ret.push(f.step(&mut p));
    }
    ret
}

fn usage() {
    println!("W: write current framebuffer to \"out.png\"");
    println!("R: re-randomize parameters");
    println!("C: clear screen");
    println!("Q: exit");
    println!("Z/Y: decrease number of elements in flame");
    println!("X: increase number of elements in flame");
    println!("A: decrease number of transforms per element");
    println!("S: increase number of transforms per element");
    println!("M: mutate current flame");
    println!("E: start/stop rendering animation");
    println!("Up/Down: in-/decrease exposure");
    println!("Left/Right: in-/decrease gamma");
}

fn main() -> ! {
    usage();
    let points_per_frame = 1000;
    let it_per_point = 1024;
    let mut cnt: usize;
    let mut buffer = Buffer::new(1024, 4.0);
    let mut f: Flame;
    let mut spp: f64;
    let mut f_n = 50;
    let mut f_m = 50;
    let mut animate: bool = false;
    loop {
        buffer.clear();
        f = Flame::new(f_n, f_m);
        cnt = 0;
        loop {
            let points = compute_point(&f, it_per_point);
            buffer.plot_v(&points);
            if animate && cnt > 0 && cnt % points_per_frame == 0 {
                let n = cnt / 1000;
                buffer.save(&format!("./out/{:05}.png", n));
                buffer.clear();
                f.mutate(0.2);
            };
            if cnt % 10 == 0 {
                spp = buffer.spp();
                set_title(&mut buffer, cnt, spp, (f_n, f_m), animate);
                buffer.render();
                if buffer.keydown(Key::W) {
                    buffer.save("out.png");
                }
                if buffer.keydown(Key::E) {
                    animate = !animate;
                    if animate {
                        buffer.clear();
                        cnt = 0;
                    }
                }
                if buffer.keydown(Key::C) {
                    buffer.clear();
                    cnt = 0;
                }
                if buffer.keydown(Key::M) {
                    buffer.clear();
                    cnt = 0;
                    f.mutate(2.0);
                }
                if buffer.keydown(Key::R) {
                    break;
                }
                if buffer.keydown(Key::Q) {
                    std::process::exit(0);
                }
                if buffer.keydown(Key::Z) || buffer.keydown(Key::Y) {
                    if f_n > 1 {
                        f_n -= 1;
                        break;
                    }
                }
                if buffer.keydown(Key::X) {
                    f_n += 1;
                    break;
                }
                if buffer.keydown(Key::A) {
                    if f_m > 1 {
                        f_m -= 1;
                        break;
                    }
                }
                if buffer.keydown(Key::S) {
                    f_m += 1;
                    break;
                }
                if buffer.keydown(Key::Up) {
                    buffer.exposure += 0.1;
                }
                if buffer.keydown(Key::Down) {
                    buffer.exposure -= 0.1;
                }
                if buffer.exposure < 0.0 {
                    buffer.exposure = 0.0;
                }
                if buffer.keydown(Key::Right) {
                    buffer.gamma += 0.1;
                }
                if buffer.keydown(Key::Left) {
                    if buffer.gamma > 0.0 {
                        buffer.gamma -= 0.1;
                    }
                }
            }
            cnt += 1;
        }
    }
}

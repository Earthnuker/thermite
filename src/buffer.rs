extern crate image;
extern crate minifb;
use self::image::{save_buffer, ColorType};
use self::minifb::{Key, Window, WindowOptions};
use super::Color;
use super::Point;
use std::cmp::min;
pub struct Buffer {
    win: Window,
    size: usize,
    img: Vec<Vec<(Color, usize)>>,
    buffer: Vec<u32>,
    pub gamma: f64,
    pub exposure: f64,
    zoom: f64,
}

impl Buffer {
    pub fn new(size: usize, zoom: f64) -> Buffer {
        let mut wo = WindowOptions::default();
        wo.borderless = false;
        wo.title = false;
        wo.resize = false;
        let win = Window::new("", size, size, wo).unwrap();
        Buffer {
            zoom: zoom,
            win: win,
            size: size,
            img: vec![vec![(Color::new(0.0, 0.0, 0.0), 0); size]; size],
            buffer: vec![0; size * size],
            gamma: 0.5,
            exposure: 3.5,
        }
    }
    pub fn clear(&mut self) {
        self.buffer = vec![0; self.size * self.size];
        self.img = vec![vec![(Color::new(0.0, 0.0, 0.0), 0); self.size]; self.size];
    }
    pub fn update(&mut self) {
        self.win.update();
    }
    fn max_alpha(&self) -> usize {
        let mut max_alpha: usize = 0;
        for py in 0..self.size {
            for px in 0..self.size {
                let n = self.img[py][px].1;
                if n > max_alpha {
                    max_alpha = n;
                }
            }
        }
        max_alpha
    }
    pub fn save(&self, filename: &str) {
        let mut out_buff: Vec<u8> = Vec::new();
        for val in &self.buffer {
            out_buff.push((((val & 0x00ff0000) >> 16) & 0xff) as u8);
            out_buff.push((((val & 0x0000ff00) >> 8) & 0xff) as u8);
            out_buff.push((((val & 0x000000ff) >> 0) & 0xff) as u8);
        }
        save_buffer(
            filename,
            &out_buff,
            self.size as u32,
            self.size as u32,
            ColorType::RGB(8),
        ).unwrap();
    }

    pub fn render(&mut self) {
        let max_alpha = self.max_alpha();
        for py in 0..self.size {
            for px in 0..self.size {
                let offset = py * self.size + px;
                let (c, n) = &self.img[py][px];
                let alpha = (*n as f64).log(2.0) / (max_alpha as f64).log(2.0);
                self.buffer[offset] = rgb(c.r, c.g, c.b, alpha, self.gamma, self.exposure);
            }
        }
        self.win.update_with_buffer(&self.buffer).unwrap();
    }
    pub fn value(&self) -> f64 {
        let mut ret: f64 = 0.0;
        for py in 0..self.size {
            for px in 0..self.size {
                let (c, _) = &self.img[py][px];
                ret += (c.r + c.g + c.b) / 3.0;
            }
        }
        ret / (self.max_alpha() as f64)
    }

    pub fn spp(&self) -> f64 {
        let mut ret: f64 = 0.0;
        for py in 0..self.size {
            for px in 0..self.size {
                let (_, n) = &self.img[py][px];
                ret += *n as f64;
            }
        }
        ret / ((self.size * self.size) as f64)
    }
    pub fn plot(&mut self, p: &Point) {
        let s = self.size as f64;
        let px = ((p.x / self.zoom + 0.5) * s) as usize;
        let py = ((p.y / self.zoom + 0.5) * s) as usize;
        if px >= self.size || py >= self.size {
            return;
        }
        self.img[py][px].0.mix(&p.c);
        self.img[py][px].1 += 1;
    }
    pub fn plot_v(&mut self, p_v: &Vec<Point>) {
        for p in p_v {
            self.plot(&p);
        }
    }
    pub fn keydown(&mut self, k: Key) -> bool {
        self.win.update();
        let ret = self.win.is_key_down(k);
        while self.win.is_key_down(k) {
            self.win.update();
        }
        return ret;
    }
    pub fn title(&mut self, title: &str) {
        self.win.set_title(title);
    }
}

fn rgb(r: f64, g: f64, b: f64, alpha: f64, gamma: f64, exposure: f64) -> u32 {
    let r = min((r * exposure * alpha.powf(1.0 / gamma) * 255.0) as u32, 255);
    let g = min((g * exposure * alpha.powf(1.0 / gamma) * 255.0) as u32, 255);
    let b = min((b * exposure * alpha.powf(1.0 / gamma) * 255.0) as u32, 255);
    (r << 16) | (g << 8) | b
}

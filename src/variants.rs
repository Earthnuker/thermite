extern crate rand;
use self::rand::distributions::Distribution;
use self::rand::Rng;
use super::Affine;
use super::Point;
use std::f64::consts::PI;
type VarFunc = fn(&Point, &Affine) -> (f64, f64);

#[allow(unused_variables)]
fn rand_var() -> VarFunc {
    let mut rng = rand::thread_rng();
    let v: Vec<VarFunc> = vec![
        (|p: &Point, a: &Affine| (p.x, p.y)),
        (|p: &Point, a: &Affine| (p.x.sin(), p.y.cos())),
        (|p: &Point, a: &Affine| {
            let m = 1.0 / r(p).powf(2.0);
            (p.x * m, p.y * m)
        }),
        (|p: &Point, a: &Affine| {
            let m = 1.0 / r(p).powf(2.0);
            (p.x * m, p.y * m)
        }),
        (|p: &Point, a: &Affine| {
            let r2 = r(p).powf(2.0);
            (
                p.x * r2.sin() - p.y * r2.cos(),
                p.x * r2.cos() - p.y * r2.sin(),
            )
        }),
        (|p: &Point, a: &Affine| {
            let m = 1.0 / r(p);
            (m * (p.x - p.y) * (p.x + p.y), m * (2.0 * p.x * p.y))
        }),
        (|p: &Point, a: &Affine| (theta(p) / PI, r(p) - 1.0)),
        (|p: &Point, a: &Affine| {
            let px = (theta(p) + r(p)).sin();
            let py = (theta(p) - r(p)).cos();
            (r(p) * px, r(p) * py)
        }),
        (|p: &Point, a: &Affine| {
            let px = (theta(p) * r(p)).sin();
            let py = -(theta(p) * r(p)).cos();
            (r(p) * px, r(p) * py)
        }),
        (|p: &Point, a: &Affine| {
            let m = theta(p) / PI;
            let px = (PI * r(p)).sin();
            let py = (PI * r(p)).cos();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let m = 1.0 / r(p);
            let px = theta(p).cos() + r(p).sin();
            let py = theta(p).sin() - r(p).cos();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let px = theta(p).sin() / r(p);
            let py = theta(p).cos() * r(p);
            (px, py)
        }),
        (|p: &Point, a: &Affine| {
            let px = theta(p).sin() * r(p).cos();
            let py = theta(p).cos() * r(p).sin();
            (px, py)
        }),
        (|p: &Point, a: &Affine| {
            let p_0 = (theta(p) + r(p)).sin();
            let p_1 = (theta(p) - r(p)).cos();
            let m = r(p);
            let px = p_0.powf(3.0) + p_1.powf(3.0);
            let py = p_0.powf(3.0) - p_1.powf(3.0);
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let p_0 = (theta(p) + r(p)).sin();
            let p_1 = (theta(p) - r(p)).cos();
            let m = r(p);
            let px = p_0.powf(3.0) + p_1.powf(3.0);
            let py = p_0.powf(3.0) - p_1.powf(3.0);
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let m = r(p).powf(0.5);
            let px = (theta(p) / 2.0 + omega()).cos();
            let py = (theta(p) / 2.0 + omega()).sin();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let x = p.x;
            let y = p.y;
            if x >= 0.0 && y >= 0.0 {
                return (x, y);
            }
            if x < 0.0 && y >= 0.0 {
                return (2.0 * x, y);
            }
            if x >= 0.0 && y < 0.0 {
                return (x, y / 2.0);
            }
            if x < 0.0 && y < 0.0 {
                return (2.0 * x, y / 2.0);
            }
            return (x, y);
        }),
        (|p: &Point, a: &Affine| {
            let dx = a.b * (p.y / a.c.powf(2.0)).sin();
            let dy = a.e * (p.x / a.f.powf(2.0)).sin();
            (p.x + dx, p.y + dy)
        }),
        (|p: &Point, a: &Affine| {
            let m = 2.0 / (r(p) + 1.0);
            let px = (theta(p) / 2.0 + omega()).cos();
            let py = (theta(p) / 2.0 + omega()).sin();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let dx = a.c * (p.y * 3.0).tan().sin();
            let dy = a.f * (p.x * 3.0).tan().sin();
            (p.x + dx, p.y + dy)
        }),
        (|p: &Point, a: &Affine| {
            let m = (p.x - 1.0).exp();
            let px = (PI * p.x).cos();
            let py = (PI * p.y).sin();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let m = r(p).powf(theta(p).sin());
            let px = theta(p).cos();
            let py = theta(p).sin();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let px = (PI * p.x).cos() * p.y.cosh();
            let py = -(PI * p.x).sin() * p.y.sinh();
            (px, py)
        }),
        (|p: &Point, a: &Affine| {
            let m = (r(p) + a.c.powf(2.0))
                .mod_euc(2.0 * a.c.powf(2.0) - a.c.powf(2.0) + r(p) * (1.0 - a.c.powf(2.0)));
            let px = theta(p).cos();
            let py = theta(p).sin();
            (m * px, m * py)
        }),
        (|p: &Point, a: &Affine| {
            let t = PI * a.c.powf(2.0);
            let switch = (theta(p) + a.f) > t / 2.0;
            let (px, py) = match switch {
                true => ((theta(p) - t / 2.0).cos(), (theta(p) - t / 2.0).sin()),
                false => ((theta(p) + t / 2.0).cos(), (theta(p) + t / 2.0).sin()),
            };
            (r(p) * px, r(p) * py)
        }),
        (|p: &Point, a: &Affine| {
            let m = 2.0 / (r(p) + 1.0);
            (m * p.x, m * p.y)
        }),
        (|p: &Point, a: &Affine| {
            let m = 4.0 / (r(p).powf(2.0) + 4.0);
            (m * p.x, m * p.y)
        }),
        (|p: &Point, a: &Affine| (p.x.sin(), p.y)),
        (|p: &Point, a: &Affine| {
            let px = p.x.sin() / p.y.cos();
            let py = p.y.tan();
            (px, py)
        }),
        // (|p: &Point, a: &Affine| {
        //     let p1 = psi();
        //     let p2 = psi();
        //     let px = p.x * (2.0 * PI * p2).cos();
        //     let py = p.y * (2.0 * PI * p2).sin();
        //     (p1 * px, p1 * py)
        // // }),
        // (|p: &Point, a: &Affine| {
        //     let p1 = psi();
        //     let p2 = psi();
        //     let px = (2.0 * PI * p2).cos();
        //     let py = (2.0 * PI * p2).sin();
        //     (p1 * px, p1 * py)
        // // }),
        // (|p: &Point, a: &Affine| {
        //     let mut rng = rand::thread_rng();
        //     let normal = rand::distributions::Normal::new(0.0, 1.0);
        //     let g = normal.sample(&mut rng);
        //     let p = psi();
        //     let px = (2.0 * PI * p).cos();
        //     let py = (2.0 * PI * p).sin();
        //     (g * px, g * py)
        // }),
        // (|p: &Point, a: &Affine| {
        //     let px = psi() - 0.5;
        //     let py = psi() - 0.5;
        //     (px, py)
        // }),
        (|p: &Point, a: &Affine| {
            let m = (1.0 / (p.x.powf(2.0) - p.y.powf(2.0)).powf(2.0)).powf(0.5);
            (m * p.x, m * p.y)
        }),
    ];
    let r = rng.choose(&v).unwrap();
    return r.clone();
}

pub struct Variant {
    pub f: VarFunc,
    pub w: f64,
}

impl Variant {
    pub fn rand() -> Variant {
        let mut rng = rand::thread_rng();
        Variant {
            f: rand_var(),
            w: rng.gen_range(0.0, 1.0),
        }
    }
}

fn r(p: &Point) -> f64 {
    (p.x * p.x + p.y * p.y).powf(0.5)
}

fn theta(p: &Point) -> f64 {
    (p.x / p.y).atan()
}
/*
fn phi(p: &Point) -> f64 {
    (p.y / p.x).atan()
}
*/

fn omega() -> f64 {
    let mut rng = rand::thread_rng();
    if rng.gen_bool(0.5) {
        PI
    } else {
        0.0
    }
}

fn psi() -> f64 {
    let mut rng = rand::thread_rng();
    rng.gen_range(0.0, 1.0)
}

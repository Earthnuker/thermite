extern crate rand;
use self::rand::distributions::Distribution;
use self::rand::Rng;
use super::Element;
use super::Point;
pub struct Flame {
    elements: Vec<(f64, Element)>,
    w_total: f64,
    fin: Element,
}

impl Flame {
    pub fn new(num_elements: usize, elem_size: usize) -> Flame {
        let mut rng = rand::thread_rng();
        let mut v = Vec::new();
        let mut t = 0f64;
        for _ in 0..num_elements {
            let w = rng.gen_range(0.0, 1.0);
            v.push((w, Element::new(elem_size)));
            t += w;
        }
        Flame {
            elements: v,
            w_total: t,
            fin: Element::new(elem_size),
        }
    }
    pub fn mutate(&mut self, s: f64) {
        self.w_total = 0.0;
        let mut rng = rand::thread_rng();
        let normal = rand::distributions::Normal::new(0.0, s);
        while self.w_total <= 0.0 {
            for el in &mut self.elements {
                //el.1.mutate(s);
                el.0 += normal.sample(&mut rng);
                self.w_total += el.0;
            }
        }
        //self.fin.mutate(s);
    }
    pub fn step(&self, p: &mut Point) -> Point {
        let mut rng = rand::thread_rng();
        let mut t = rng.gen_range(0.0, self.w_total);
        for (w, f) in &self.elements {
            t -= w;
            if t <= 0.0 {
                f.step(p);
                let mut ret = p.clone();
                self.fin.step(&mut ret);
                return ret;
            }
        }
        panic!("Nope!");
    }
}

extern crate rand;
use self::rand::distributions::Distribution;
use self::rand::Rng;
use super::Color;
#[derive(Debug, PartialEq)]
pub struct Affine {
    pub a: f64,
    pub b: f64,
    pub c: f64,
    pub d: f64,
    pub e: f64,
    pub f: f64,
    pub color: Color,
}

impl Affine {
    pub fn new(x: (f64, f64, f64), y: (f64, f64, f64)) -> Affine {
        Affine {
            a: x.0,
            b: x.1,
            c: x.2,
            d: y.0,
            e: y.1,
            f: y.2,
            color: Color::rand(),
        }
    }
    fn is_contractive(&self) -> bool {
        let (a, b, c, d, _, _) = (self.a, self.b, self.c, self.d, self.e, self.f);
        (a * a + c * c < 1.0)
            && (b * b + d * d < 1.0)
            && ((a * a + b * b + c * c + d * d) - (a * d - b * c) * (a * b - b * c)) < 1.0
    }
    pub fn mutate(&mut self, s: f64) {
        let mut rng = rand::thread_rng();
        let normal = rand::distributions::Normal::new(0.0, s);
        self.a += normal.sample(&mut rng);
        self.b += normal.sample(&mut rng);
        self.c += normal.sample(&mut rng);
        self.d += normal.sample(&mut rng);
        self.e += normal.sample(&mut rng);
        self.f += normal.sample(&mut rng);
    }
    pub fn rand(contract: bool) -> Affine {
        let mut rng = rand::thread_rng();
        loop {
            let a = Affine::new(
                (
                    rng.gen_range(-1.0, 1.0),
                    rng.gen_range(-1.0, 1.0),
                    rng.gen_range(-1.0, 1.0),
                ),
                (
                    rng.gen_range(-1.0, 1.0),
                    rng.gen_range(-1.0, 1.0),
                    rng.gen_range(-1.0, 2.0),
                ),
            );
            if !contract && !a.is_contractive() {
                return a;
            }
            if contract && a.is_contractive() {
                return a;
            }
        }
    }
}

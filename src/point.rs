extern crate rand;
use self::rand::Rng;
use super::Affine;
use super::Color;
use super::Variant;
#[derive(Debug, PartialEq, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
    pub c: Color,
}

impl Point {
    pub fn new(x: f64, y: f64) -> Point {
        Point {
            x,
            y,
            c: Color::rand(),
        }
    }
    pub fn mix(&mut self, other: &Color) {
        self.c.mix(other)
    }

    pub fn rand() -> Point {
        let mut rng = rand::thread_rng();
        let px = rng.gen_range(-1f64, 1f64);
        let py = rng.gen_range(-1f64, 1f64);
        Point::new(px, py)
    }
    pub fn apply(&mut self, a: &Affine) {
        let px = a.a * self.x + a.b * self.y + a.c;
        let py = a.d * self.x + a.e * self.y + a.f;
        self.x = px;
        self.y = py;
        self.mix(&a.color);
    }
    pub fn var(&mut self, v: &Variant, a: &Affine) {
        let (px, py) = (v.f)(self, a);
        self.x = px;
        self.y = py;
    }
    pub fn step(&mut self, v: &Variant, a: &Affine) {
        self.apply(a);
        self.var(v, a);
    }
}

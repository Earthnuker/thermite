extern crate rand;
use self::rand::Rng;
use super::Affine;
use super::Point;
use super::Variant;
pub struct Element {
    variants: Vec<(f64, Variant)>,
    affine: Affine,
    post: Affine,
    w_total: f64,
}

impl Element {
    pub fn new(n: usize) -> Element {
        let mut rng = rand::thread_rng();
        let mut v_v = Vec::new();
        let mut w_total = 0f64;
        for _ in 0..n {
            let w = rng.gen_range(0.0, 1.0);
            v_v.push((w, Variant::rand()));
            w_total += w;
        }
        Element {
            variants: v_v,
            affine: Affine::rand(false),
            post: Affine::rand(false),
            w_total,
        }
    }

    pub fn mutate(&mut self, s: f64) {
        self.affine.mutate(s);
        self.post.mutate(s);
    }

    pub fn step(&self, p: &mut Point) {
        let mut px = 0f64;
        let mut py = 0f64;
        for (w, var) in &self.variants {
            let mut p_ = p.clone();
            p_.step(var, &self.affine);
            px += p_.x * w;
            py += p_.y * w;
        }
        p.mix(&self.affine.color);
        p.x = px / self.w_total;
        p.y = py / self.w_total;
        p.apply(&self.post);
    }
}

extern crate rand;
use self::rand::Rng;
#[derive(Debug, PartialEq, Clone)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

impl Color {
    pub fn new(r: f64, g: f64, b: f64) -> Color {
        Color { r, g, b }
    }

    pub fn rand() -> Color {
        let mut rng = rand::thread_rng();
        Color {
            r: rng.gen_range(0.25, 1.0),
            g: rng.gen_range(0.25, 1.0),
            b: rng.gen_range(0.25, 1.0),
        }
    }

    pub fn mix(&mut self, other: &Color) {
        self.r = (self.r + other.r) / 2.0;
        self.g = (self.g + other.g) / 2.0;
        self.b = (self.b + other.b) / 2.0;
    }
}
